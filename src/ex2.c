#include <stdio.h>
int main()
{
    printf("Int: %lu\n", sizeof(int));
    printf("Char: %lu\n", sizeof(char));
    printf("float: %lu\n", sizeof(float));
    printf("double: %lu\n", sizeof(double));
    printf("short int: %lu\n", sizeof(short int));
    printf("long int: %lu\n", sizeof(long int));

    return 0;
}
