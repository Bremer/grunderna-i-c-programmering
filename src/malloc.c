#include <stdlib.h>
#include <stdio.h>
char *test_malloc();
int main()
{
    char *namn = test_malloc();
    printf("Ditt namn är %s.\n", namn);
    free(namn);

    return 0;
}

char *test_malloc()
{
    char *namn = malloc(20 * sizeof(char));
    printf("Skriv ditt namn: ");
    scanf("%19s", namn);

    return namn;
}
