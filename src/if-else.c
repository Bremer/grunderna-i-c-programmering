#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    time_t t;
    int number;

    srand((unsigned) time(&t));
    number = rand() % 100;

    if (number >= 50)
        printf("Nummer över eller likamed  50\n");
    else
        printf("Nummer under 50\n");

    return 0;
}
