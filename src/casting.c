#include <stdio.h>

int main()
{
    char tecken = 'A';
    int nummer = 100;
    float flyttal = 3.14;

    printf("Omvandla en char(%c) till en int: %d\n", tecken, (int)tecken);
    printf("omvandla en int(%i) till en char: %c\n", nummer, (char)nummer);
    printf("Omvandla ett flyttal(%f) till en int: %i\n", flyttal, (int)flyttal);

    return 0;
}
