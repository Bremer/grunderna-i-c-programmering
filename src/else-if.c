#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    time_t t;
    int number;

    srand((unsigned) time(&t));
    number = rand() % 100;

    if (number <= 25)
        printf("Nummer under eller likamed 25\n");
    else if  (number <= 50)
        printf("Nummer under eller likamed 75\n");
    else if (number <= 75)
        printf("Nummer under eller likamed 75\n");
    else
        printf("Nummer under eller likamed 100\n");

    return 0;
}
