#include <stdio.h>

int main()
{
    char c = 'a';
    char s[] = {'s','t','r','i','n','g'};
    
    printf("Char: %c\n", c);
    printf("String: %s\n", s);
    
    return 0;
}
