#include <stdlib.h>
#include <stdio.h>
char *test_calloc();
int main()
{
    char *namn = test_calloc();
    printf("Ditt namn är %s\n", namn);
    free(namn);

    return 0;
}

char *test_calloc()
{
    char *namn = calloc(20, sizeof(char));

    printf("Skriv ditt namn: ");
    scanf("%19s", namn);

    return namn;
}
