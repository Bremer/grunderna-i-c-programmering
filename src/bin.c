#include <stdio.h>

void bin(int number);

int main()
{
    bin(2);
    bin(15);
    bin(31);
    bin(64);
    bin(128);

    return 0;
}

void bin(int number)
{
    int binary[8] = {0,0,0,0,0,0,0,0};
    int i = 0;
    int j = 7;

    while (number)
    {
        binary[i] = number % 2;
        number = number / 2;
        i++;
    }

    for(;j > -1; j--)
        printf("%i", binary[j]);
    printf("\n");
}
