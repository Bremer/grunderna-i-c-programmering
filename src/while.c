#include <stdio.h>

int main()
{
    char a[] = {'a','b','c','d'};
    int x = 0;

    while(a[x] != '\0')
    {
        printf("%c\n", a[x++]);
    }

    return 0;
}
