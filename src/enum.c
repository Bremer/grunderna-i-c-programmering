#include <stdio.h>

enum boolean {false, true};

int main()
{
	enum boolean bl;

	bl = false;
	printf("Boolean false : %d\n", bl);

	bl = true;
	printf("Boolean true  : %d\n", bl);

	return 0;
}
