#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    int val;

    if (argc < 2)
    {
        printf("Använd: %s <integer between 0-2>\n", argv[0]);
        return 1;
    }

    val = argv[1][0] - '0';

    switch(val)
    {
        case 0:
            printf("Du matade in %i\n", val);
            break;
        case 1:
            printf("Du matade in %i\n", val);
            break;
        case 2:
            printf("Du matade in %i\n", val);
            break;
        default:
            printf("Du har matat in ett ogiltligt värde.\n");
    }

    return 0;
}
