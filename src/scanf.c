#include <stdio.h>

int main()
{
    char namn[20];
    printf("Skriv in ditt namn: ");
    scanf("%19s", namn);
    printf("Du heter %s, och variabeln är %ld bytes stor.\n", namn, sizeof(namn));

    return 0;
}
