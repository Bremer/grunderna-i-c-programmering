#include <stdio.h>
void func();

int main()
{
    int i;

    for(i = 0; i < 10; i++)
    {
        func();
    }
    return 0;
}

void func()
{
    int static y = 0;
    
    printf("%d\n", y);

    y++;
}