#include <stdio.h>
#include <assert.h>
#include "rs.h"

int main()
{
    int a, b, c, d;

    printf("Lyckade asserts\n");

    a = add(1, 1);
    assert(a == 2);

    b = sub(1, 1);
    assert(b == 0);

    c = mul(1, 1);
    assert(c == 1 );

    d = div(1, 1);
    assert(d == 1);

    printf("a:%i\nb:%i\nc:%i\nd:%i\n", a, b, c, d);

    return 0;
}
