#include <stdio.h>

int main()
{
    signed char a = 50;
    unsigned char b = -50;

    printf("signed: %c\n", a);
    printf("unsigned: %u\n", b);

    return 0;
}
