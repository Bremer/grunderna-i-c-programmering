#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person
{
    char namn[10];
    struct person *next;
};

void print(struct person *ptr);
void insert(char *namn, struct person **head);
void free_allocated_memory(struct person *head);


int main()
{
    struct person *head = NULL;

    insert("josefin", &head);
    insert("dennis", &head);
    insert("olof", &head);

    print(head);
    free_allocated_memory(head);

    return 0;
}

void insert(char *name, struct person **head)
{
    struct person *person = calloc(1, sizeof(struct person));
    strncpy(person->namn, name, strlen(name));
    person->next = *head;
    *head = person;
}

void print(struct person *ptr)
{
    while (ptr != NULL)
    {
        printf("%s\n", ptr->namn);
        ptr = ptr->next;
    }
}


void free_allocated_memory(struct person *ptr)
{
    while(ptr != NULL)
        {
            struct person *free_ptr = ptr;
            ptr = ptr->next;
            free(free_ptr);
        }
}

