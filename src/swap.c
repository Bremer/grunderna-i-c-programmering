#include <stdio.h>
void swap(int *, int*);

int main()
{
    int a = 1;
    int b = 2;
    int *x = &a;
    int *y = &b;

    printf("a : %i\nb : %i\n", a, b);
    swap(x, y);
    printf("a : %i\nb : %i\n", a, b);

    return 0;
}

void swap(int *x, int *y)
{
    int temp = *x;
    *x = *y;
    *y = temp;
}
