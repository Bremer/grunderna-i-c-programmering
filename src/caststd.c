#include <stdio.h>
#include <stdlib.h>

int main()
{
    char s[] = {'3','.','1', '4'};

    printf("Omvandla en sträng(%s) till en double: %f\n", s, atof(s));
    printf("Omvandla en sträng(%s) till ett heltal: %i\n", s, atoi(s));

    return 0;
}
