#include <stdio.h>

int main()
{
    typedef char bokstav;

    bokstav a = 'a', b = 'b';

    printf("%c %c\n", a, b);

    return 0;
}
