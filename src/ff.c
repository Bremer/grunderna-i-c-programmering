#include <stdio.h>

int main()
{
    int matris[4][3] = { {0, 1, 2},
                         {3, 4, 5},
                         {6, 7, 8},
                         {9, 10, 11}
                       };

    int rad, col;

    for(rad = 0; rad < 4; rad++)
    {
        for(col = 0; col < 3; col++)
        {
            printf("%3d", matris[rad][col]);
        }
        printf("\n");
    }
    return 0;
}
