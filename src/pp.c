#include <stdio.h>

int main()
{
    int a = 5;
    int *p = &a;
    int **dp = &p;
    printf("%i\n", **dp);

    return 0;
}
