#include <stdio.h>

void stega(char namn[]);

int main()
{
    char namn[] = "Rickard";
    stega(namn);

    return 0;
}

void stega(char namn[])
{
    int i = 0;
    while(namn[i] != '\0')
        printf("%c", namn[i++]);

    printf("\n%i steg\n", i);

    i = 0;

    while(*namn != '\0')
    {
        printf("%c", *namn++);
        i++;
    }

    printf("\n%i steg\n", i);
}
