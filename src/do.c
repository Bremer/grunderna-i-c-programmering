#include <stdio.h>

int main(int argc, char *argv[])
{
    int x = 1;
    int y = 0;

    if (argc == 1)
    {
        printf("Usage: %s och argument\n", argv[0]);
        return 1;
    }

    do
    {
        do
        {
            printf("%c", argv[x][y]);
        } while(argv[x][y++] != '\0');

        printf("\n");
        y = 0;
        x++;

    } while (x < argc);

    return 0;
}
