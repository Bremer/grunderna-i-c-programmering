#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char* foo();

int main()
{
    char *a = foo();
    printf("%s\n", a);
    free(a);

    return 0;
}

char* foo()
{
    char b[] = {'r','i','c','k','a','r','d'};
    int len = strlen(b);
    char *a = calloc(len + 1,  sizeof(char));
    strncpy(a, b, len);
    return a;
}

